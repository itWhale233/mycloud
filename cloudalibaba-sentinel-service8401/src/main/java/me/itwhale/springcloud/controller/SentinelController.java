package me.itwhale.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class SentinelController {

    @Value("${server.port}")
    private String serverPort;


    @GetMapping("/testA")
    public String testA(){

        return "Test A" + serverPort;
    }

    @GetMapping("/testB")
    public String testB(){

       log.info(Thread.currentThread().getName() + "\t" + "TestB");
        return "Test B" + serverPort;
    }


    @GetMapping("/testE")
    public String testE(){
       int i = 10/0;
        return "Test E" + serverPort;
    }

    @GetMapping("/testD")
    public String testD(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Test D" + serverPort;
    }


    @GetMapping("/testHotKey")
    @SentinelResource(value = "testHotKey",blockHandler = "del_testHotKey")
    public String testHotKey(@RequestParam(name = "p1",required = false)String p1,
                             @RequestParam(name = "p2",required = false)String p2){

        return "test hot key ";
    }

    public String del_testHotKey(String p1, String p2, BlockException e){

        return "testHotKey 兜底方法";
    }
}
