package me.itwhale.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import lombok.extern.slf4j.Slf4j;
import me.itwhale.springcloud.entities.CommonResult;
import me.itwhale.springcloud.entities.Payment;
import me.itwhale.springcloud.handle.CustomerBlockHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class RateLimitController {

    @GetMapping("/rate/test1")
    @SentinelResource(value = "Rate1")
    public CommonResult test1(){

        return new CommonResult(200,"Rate test1",new Payment(200L,"1001"));
    }

    @GetMapping("/rate/test2")
    @SentinelResource(value = "Rate2",blockHandlerClass = CustomerBlockHandler.class,blockHandler = "customerHandler2")
    public CommonResult test2(){

        return new CommonResult(200,"Rate test1",new Payment(200L,"1001"));
    }
}
