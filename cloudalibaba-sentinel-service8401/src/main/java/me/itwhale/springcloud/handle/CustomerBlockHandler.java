package me.itwhale.springcloud.handle;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import me.itwhale.springcloud.entities.CommonResult;

public class CustomerBlockHandler {
    public static CommonResult customerHandler1(BlockException blockException){

        return new CommonResult(4444,"Exception Handle ---- 111111","");
    }


    public static CommonResult customerHandler2(BlockException blockException){

        return new CommonResult(4444,"Exception Handle ---- 222222","");
    }
}
