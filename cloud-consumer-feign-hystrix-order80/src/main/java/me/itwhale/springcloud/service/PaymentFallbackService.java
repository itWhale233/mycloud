package me.itwhale.springcloud.service;

import org.springframework.stereotype.Component;

@Component
public class PaymentFallbackService implements PaymentService {
    @Override
    public String PaymentInfo_OK(Integer id) {
        return "80：OK 兜底方法，系统错误";
    }

    @Override
    public String PaymentInfo_TimeOut(Integer id) {
        return "80：timeout 兜底方法，系统错误";
    }
}
