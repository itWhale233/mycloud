package me.itwhale.springcloud.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT",fallback = PaymentFallbackService.class)
public interface PaymentService {

    @GetMapping("/payment/ok/{id}")
    public String PaymentInfo_OK(@PathVariable("id") Integer id);


    @GetMapping("/payment/timeout/{id}")
    public String PaymentInfo_TimeOut(@PathVariable("id") Integer id);


}
