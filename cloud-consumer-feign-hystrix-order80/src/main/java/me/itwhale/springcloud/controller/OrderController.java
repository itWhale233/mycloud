package me.itwhale.springcloud.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import me.itwhale.springcloud.service.PaymentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
@DefaultProperties(defaultFallback = "Payment_Global_Handle")
public class OrderController {

    @Resource
    private PaymentService paymentService;

    @GetMapping("/consumer/payment/ok/{id}")
    public String PaymentInfo_OK(@PathVariable("id") Integer id){
        return paymentService.PaymentInfo_OK(id);
    }

//    @HystrixCommand(fallbackMethod = "PaymentInfo_TimeOut_Handle",commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "1500")
//    })
    // 使用全局兜底方法时不需要单独设置fallbackMethod
    @HystrixCommand
    @GetMapping("/consumer/payment/timeout/{id}")
    public String PaymentInfo_TimeOut(@PathVariable("id") Integer id){
//        int i = 10/0;
        return paymentService.PaymentInfo_TimeOut(id);
    }


    // 自定义兜底方法
    public String PaymentInfo_TimeOut_Handle(Integer id){
        return "80：对方系统繁忙或本地内部错误，请稍后再试；";
    }

    // 新设置的该类全局兜底方法
    public String Payment_Global_Handle(){
        return "全局兜底：服务超时，请稍候重试";
    }

}
