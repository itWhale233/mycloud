package me.itwhale.springcloud.dao;

import me.itwhale.springcloud.domain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OrderDao {

    void create(Order order);

    void changeStatus(@Param("id") Long id,@Param("status") Integer status);
}
