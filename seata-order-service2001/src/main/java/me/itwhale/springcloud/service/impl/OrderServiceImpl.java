package me.itwhale.springcloud.service.impl;

import io.seata.spring.annotation.GlobalTransactional;
import me.itwhale.springcloud.dao.OrderDao;
import me.itwhale.springcloud.domain.Order;
import me.itwhale.springcloud.service.AccountService;
import me.itwhale.springcloud.service.OrderService;
import me.itwhale.springcloud.service.StorageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OrderServiceImpl implements OrderService {

    /**
     * 下订单服务 下订单 - 减库存 - 扣余额 - 改状态
     * @param order
     */

    @Resource
    private OrderDao orderDao;

    @Resource
    private StorageService storageService;

    @Resource
    private AccountService accountService;

    @Override
    @GlobalTransactional(name = "whale-create-order",rollbackFor = Exception.class)
    public void create(Order order) {

        // 下订单
        orderDao.create(order);

        // 减库存
        storageService.decrease(order.getProductId(),order.getCount());

        // 扣金额
        accountService.decrease(order.getUserId(),order.getMoney());

//        int i = 10/0;
        // 改状态
        orderDao.changeStatus(order.getId(),1);



    }
}
