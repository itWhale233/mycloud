package me.itwhale.springcloud.service;

import me.itwhale.springcloud.domain.Order;

public interface OrderService {

    void create(Order order);

}
