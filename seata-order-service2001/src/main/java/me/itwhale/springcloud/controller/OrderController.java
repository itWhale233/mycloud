package me.itwhale.springcloud.controller;

import me.itwhale.springcloud.domain.CommonResult;
import me.itwhale.springcloud.domain.Order;
import me.itwhale.springcloud.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class OrderController {

    @Resource
    private OrderService orderService;

    @GetMapping("/order/create")
    public CommonResult create(Order order){

        orderService.create(order);
        return new CommonResult(200,"订单创建成功！");
    }
}
