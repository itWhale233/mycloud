package me.itwhale.springcloud.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"me.itwhale.springcloud.dao"})
public class MyBatisConfig {


}



