package me.itwhale.springcloud.service.impl;

import me.itwhale.springcloud.dao.PaymentDao;
import me.itwhale.springcloud.entities.Payment;
import me.itwhale.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PaymentImpl implements PaymentService {

    @Resource
    private PaymentDao paymentDao;

    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}
