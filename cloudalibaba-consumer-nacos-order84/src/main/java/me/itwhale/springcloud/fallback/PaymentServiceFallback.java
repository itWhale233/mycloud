package me.itwhale.springcloud.fallback;

import me.itwhale.springcloud.entities.CommonResult;
import me.itwhale.springcloud.entities.Payment;
import me.itwhale.springcloud.service.PaymentService;
import org.springframework.stereotype.Component;

@Component
public class PaymentServiceFallback implements PaymentService {
    @Override
    public CommonResult<Payment> paymentSQL(Long id) {
        return new CommonResult<Payment>(444,"Payment OpenFeign 兜底方法",new Payment());
    }
}
