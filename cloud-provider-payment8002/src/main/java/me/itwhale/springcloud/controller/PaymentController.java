package me.itwhale.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import me.itwhale.springcloud.entities.CommonResult;
import me.itwhale.springcloud.entities.Payment;
import me.itwhale.springcloud.service.PaymentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@Slf4j
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @PostMapping(value = "/payment/create")
    public CommonResult create(@RequestBody Payment payment){

        int result = paymentService.create(payment);
        log.info("插入结果：" + result + "啦啦啦");
        if(result > 0){
            return new CommonResult(200,"插入成功" + serverPort,result);
        }else {
            return new CommonResult(500,"插入失败",null);
        }

    }


    @GetMapping(value = "/payment/get/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id){
        Payment payment = paymentService.getPaymentById(id);
        log.info("查询结果：" + payment + "哈哈哈");
        if (null != payment){
            return new CommonResult(200,"查询到结果" + serverPort,payment);
        }else{
            return new CommonResult(500,"查询不到该id" + id,null);
        }

    }
}
