package me.itwhale.springcloud.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import me.itwhale.springcloud.service.PaymentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @GetMapping("/payment/ok/{id}")
    public String PaymentInfo_OK(@PathVariable("id") Integer id){

        return paymentService.paymentInfo_OK(id);
    }

    // 配置服务降级的条件是超时3s之后
    @HystrixCommand(fallbackMethod = "paymentInfo_Timeout_Handle",commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "3000")

    })
    @GetMapping("/payment/timeout/{id}")
    public String PaymentInfo_TimeOut(@PathVariable("id") Integer id){

        return paymentService.paymentInfo_Timeout(id);
    }


    // 熔断器方法演示
    @HystrixCommand(fallbackMethod = "breaker_handle",commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "3000"),
            @HystrixProperty(name = "circuitBreaker.enabled",value = "true"),
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "10000"),
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "60")
    })
    @GetMapping("/payment/breaker/{id}")
    public String PaymentInfo_breaker(@PathVariable("id") Integer id){
        return paymentService.breaker(id);
    }


    public String paymentInfo_Timeout_Handle(Integer id) {
        return "系统繁忙，请稍后\t" + id;
    }

    public String breaker_handle(Integer id){
        return "8001:系统繁忙,breaker 方法兜底";
    }


}
