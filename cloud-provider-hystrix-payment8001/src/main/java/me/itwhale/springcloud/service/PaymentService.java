package me.itwhale.springcloud.service;

import cn.hutool.core.util.IdUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class PaymentService {

    public String paymentInfo_OK(Integer id) {
        return "线程池： " + Thread.currentThread().getName()
                + "   paymentInfo_OK,id:" + id + " 正常访问！";
    }


    public String paymentInfo_Timeout(Integer id) {

        // 亦或者 出现程序错误也会直接降级 跳转兜底方法
//        int i = 10/0;

        int timeNumber = 3;
        try {
            TimeUnit.SECONDS.sleep(timeNumber);
        } catch (Exception e){
            e.printStackTrace();
        }
        return "线程池： " + Thread.currentThread().getName()
                + "   paymentInfo_Timeout,id:" + id + " 耗时(秒):" + timeNumber;
    }




    public String breaker(Integer id){

        if(id < 0){
            throw new RuntimeException("id 不能为负数");
        }
            return "8001：访问成功," + Thread.currentThread().getName() + "\t" + IdUtil.simpleUUID();

    }



    }
