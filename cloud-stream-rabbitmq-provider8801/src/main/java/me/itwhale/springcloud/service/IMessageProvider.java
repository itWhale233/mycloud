package me.itwhale.springcloud.service;

public interface IMessageProvider {
    public String send();
}
