package me.itwhale.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import me.itwhale.springcloud.service.IMessageProvider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class MessageProviderController {

    @Resource
    private IMessageProvider messageProvider;

    @GetMapping("/sendMessage")
    public String sendMessage(){
        messageProvider.send();
        return "success";
    }
}
